package com.airline.canteenmanager.service;

import com.airline.canteenmanager.entity.Canteen;
import com.airline.canteenmanager.eums.Menu;
import com.airline.canteenmanager.model.CanteenItem;
import com.airline.canteenmanager.model.CanteenRequest;
import com.airline.canteenmanager.repository.CanteenRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CanteenService {
    private final CanteenRepository canteenRepository;

    public Integer setCanteen(CanteenRequest request) {
        Canteen addData = new Canteen();
        addData.setMenu(request.getMenu());
        addData.setNumber(request.getNumber());
        addData.setPaymentMethod(request.getPaymentMethod());
        addData.setPrice(request.getMenu().getPrice());
        addData.setSum(request.getMenu().getPrice() * request.getNumber());

        canteenRepository.save(addData);

        return request.getMenu().getPrice() * request.getNumber();
    }

    public List<CanteenItem> getCanteens() {
        List<Canteen> originList = canteenRepository.findAll();
        List<CanteenItem> result = new LinkedList<>();

        for (Canteen item : originList) {
            CanteenItem addItem = new CanteenItem();
            addItem.setId(item.getId());
            addItem.setMenu(item.getMenu().getMenuName());
            addItem.setPrice(item.getPrice());
            addItem.setPaymentMethod(item.getPaymentMethod());
            addItem.setNumber(item.getNumber());
            addItem.setSum(item.getSum());

            result.add(addItem);
        }
        return result;
    }
}
