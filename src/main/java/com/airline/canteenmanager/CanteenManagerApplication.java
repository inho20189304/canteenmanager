package com.airline.canteenmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CanteenManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CanteenManagerApplication.class, args);
	}

}
