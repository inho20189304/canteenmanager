package com.airline.canteenmanager.model;

import com.airline.canteenmanager.eums.Menu;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CanteenRequest {
    @NotNull
    private Integer number;
    @NotNull
    @Length(min = 2, max = 5)
    private String paymentMethod;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Menu menu;


}
