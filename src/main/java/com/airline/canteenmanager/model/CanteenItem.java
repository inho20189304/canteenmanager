package com.airline.canteenmanager.model;

import com.airline.canteenmanager.eums.Menu;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.web.reactive.context.StandardReactiveWebEnvironment;
import org.springframework.stereotype.Service;

@Getter
@Setter
public class CanteenItem {
    private Long id;
    private String menu;

    private Integer price;
    private String paymentMethod;
    private Integer number;

    private Integer sum;
}
