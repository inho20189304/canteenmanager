package com.airline.canteenmanager.controller;

import com.airline.canteenmanager.model.CanteenItem;
import com.airline.canteenmanager.model.CanteenRequest;
import com.airline.canteenmanager.service.CanteenService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/canteen")
public class CanteenController {
    private final CanteenService canteenService;

    @PostMapping("/price")
    public Integer setMenu(@RequestBody @Valid CanteenRequest request) {
        int result = canteenService.setCanteen(request);

    return result;
    }
    @GetMapping("/all")
    public List<CanteenItem> getCanteens() {
        List<CanteenItem> result = canteenService.getCanteens();

        return result;
    }
}
