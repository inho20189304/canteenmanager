package com.airline.canteenmanager.entity;

import com.airline.canteenmanager.eums.Menu;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Canteen {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private Menu menu;

    @Column(nullable = false)
    private Integer number;


    @Column(nullable = false)
    private Integer price;

    @Column(nullable = false, length = 5)
    private String paymentMethod;

    @Column(nullable = false)
    private Integer sum;
}
