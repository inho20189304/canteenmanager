package com.airline.canteenmanager.repository;

import com.airline.canteenmanager.entity.Canteen;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CanteenRepository extends JpaRepository<Canteen, Long> {
}
