package com.airline.canteenmanager.eums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Menu {
    DDEKBOKKI("떡볶이", 4000),
    HOTDOG("힛도그", 2000),
    FISHCAKE("어묵", 700),
    SUNDAE("순대", 4000),
    CUPRAMEN("컵라면", 1000),
    FRIED("튀김", 2000);

    private final String menuName;
    private final Integer price;
}
